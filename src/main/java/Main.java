import dijkstra.Graph;
import queue.MyPriorityQueue;

/**
 * Created by amen on 9/7/17.
 */
public class Main {
    public static void main(String[] args) {
        MyPriorityQueue<Integer> queue = new MyPriorityQueue<Integer>();

        queue.add(2, 1.0);
        queue.add(3, 2.0);
        queue.add(5, 3.0);
        queue.add(6, 10.0);
        queue.add(7, 15.0);
        queue.add(1, 0.1);
        queue.add(4, 2.2);

        queue.print();

        System.out.println(queue.pop());

        Graph g = new Graph();
        g.addNode(1, "Gdansk");
        g.addNode(2, "Warszawa");
        g.addNode(3, "Krakow");
        g.addNode(4, "Gdynia");
        g.addNode(5, "Sopot");
        g.addNode(6, "Koszalin");
        g.addNode(7, "Katowice");
        g.addNode(8, "Rzeszów");
        g.addNode(10, "Radom");

        // krawedzie z 1
        g.addEdge(1, 2, 2.0);
        g.addEdge(1, 5, 3.0);
        g.addEdge(1, 4, 2.0);
        g.addEdge(1, 7, 1.0);
        g.addEdge(1, 6, 2.0);
        g.addEdge(1, 8, 1.0);

        // krawedzie z 3
        g.addEdge(3, 4, 7.0);
        g.addEdge(3, 6, 2.0);

        // krawedzie z 2
        g.addEdge(5, 10, 5.0);
        g.addEdge(5, 8, 3.0);
        g.addEdge(5, 4, 1.0);

        // krawedzie z 6
        g.addEdge(7, 4, 99.0);


        g.dijkstra(1, 10);
    }
}
