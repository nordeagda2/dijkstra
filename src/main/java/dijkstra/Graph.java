package dijkstra;

import queue.MyPriorityQueue;
import queue.QueueElement;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by amen on 9/7/17.
 */
public class Graph {

    private Map<Integer, Node> nodes = new HashMap<Integer, Node>();

    /**
     * Dodajemy
     * @param id
     * @param name
     */
    public void addNode(int id, String name) {
        Node node = new Node(id, name);
        nodes.put(id, node);
    }

    public void addEdge(int id_from, int id_to, double distance) {
        // jesli wierzcholki istnieja
        if (nodes.containsKey(id_from) && nodes.containsKey(id_to)) {
            nodes.get(id_from).addEdge(id_to, distance); // dodaj krawedz z wierzcholka a do b
            nodes.get(id_to).addEdge(id_from, distance); // dodaj krawedz z wierzcholka b do a
        }
    }

    public void dijkstra(int id_begin, int id_finish) {
        if (!nodes.containsKey(id_begin) ||         // jesli wierzcholki(początek lub koniec) nie istnieja to koncze algorytm
                !nodes.containsKey(id_finish)) {
            System.out.println("Error, nodes does not exist.");
            return;     // powrot
        }

        // tworzymy kolejke priorytetowa dla algorytmu
        MyPriorityQueue<Node> priorityQueue = new MyPriorityQueue<Node>();

        nodes.get(id_begin).setDistance(0.0); // ustawiam dystans dla wierzchołka początkowego na 0

        priorityQueue.add(nodes.get(id_begin), 0.0); // umieszczamy wierzcholek poczatkowy w kolejce

        while (!priorityQueue.isEmpty()) { // dopóki w kolejce są elementy
            Node obecnyWierzcholek = priorityQueue.pop();    // pobierz z kolejki element o najniższym dystansie/priorytecie
            if (obecnyWierzcholek.getId() == id_finish) {
                // jesli dotarlismy do ostatniego wierzcholka, to nie musimy juz przegladac grafu.
                break;
            }

            // pobieramy wierzchołek z góry kolejki priorytetowej
            for (Edge krawedzDoSasiada : obecnyWierzcholek.getNeighbours()) { // dla kazdego z sasiadow
                int idSasiada = krawedzDoSasiada.getId_to(); // bierzemy id sasiada

                Node sprawdzany_sasiad = nodes.get(idSasiada); // pobieram z mapy sąsiada

                double obecnyDystansSasiada = sprawdzany_sasiad.getDistance(); // obecny dystans sasiada do wierzcholka poczatkowego
                // poniżej obliczamy sumę: (dystans_sasiada_od_startu + waga_krawedzi)
                double sumaDystansObecnyPlusKrawedz = (obecnyWierzcholek.getDistance() + krawedzDoSasiada.getDistance());

                // poniżej: jesli powyzsza suma jest wyzsza (czyli znalezlismy krotsza sciezke niz ta istniejaca)
                if (obecnyDystansSasiada > sumaDystansObecnyPlusKrawedz) {
                    sprawdzany_sasiad.setDistance(sumaDystansObecnyPlusKrawedz); // ustawiamy sąsiadowi krótszy dystans

                    // dodajemy/przekazujemy sąsiadowi obecną trasę do niego, oraz dopisujemy do niej nazwe sasiada
                    sprawdzany_sasiad.setVisitedNodes(obecnyWierzcholek.getVisitedNodes() + " -> " + sprawdzany_sasiad.getName());

                    // dodajemy sąsiada z powrotem do kolejki priorytetowej, gdzie priorytetem jest jego obecny dystans od startu
                    priorityQueue.add(sprawdzany_sasiad, sprawdzany_sasiad.getDistance()); // dodajemy do kolejki priorytetowej
                }
            }
        }

        // wypisanie dystansu oraz trasy.
        System.out.println("Dystans: " + nodes.get(id_finish).getDistance());
        System.out.println("Trasa: " + nodes.get(id_finish).getVisitedNodes());


        // TODO: czyszczenie - każdemu wierzchołkowi ustawić dystans infinity
        // TODO: każdemu wierzchołkowi ustawić visitedNodes = jego nazwę miasta (jego name)
    }

}
