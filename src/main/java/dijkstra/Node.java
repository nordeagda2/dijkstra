package dijkstra;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by amen on 9/7/17.
 */
public class Node {
    private int id;

    private Double distance;
    private boolean visited;
    private String name;

    private Set<Edge> neighbours;

    private String visitedNodes;

    public Node(int id, String name) {
        this.id = id;
        this.name = name;
        this.distance = Double.POSITIVE_INFINITY;
        this.visited = false;

        this.neighbours = new LinkedHashSet<Edge>();

        this.visitedNodes = name;
    }

    public String getVisitedNodes() {
        return visitedNodes;
    }

    public void setVisitedNodes(String visitedNodes) {
        this.visitedNodes = visitedNodes;
    }

    public void addEdge(int to_node, double distance) {
        neighbours.add(new Edge(id, to_node, distance));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Edge> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(Set<Edge> neighbours) {
        this.neighbours = neighbours;
    }
}
