package dijkstra;

/**
 * Created by amen on 9/7/17.
 */
public class Edge {

    private int id_from;
    private int id_to;
    private Double distance;

    public Edge(int id_from, int id_to, Double distance) {
        this.id_from = id_from;
        this.id_to = id_to;
        this.distance = distance;
    }

    public int getId_from() {
        return id_from;
    }

    public void setId_from(int id_from) {
        this.id_from = id_from;
    }

    public int getId_to() {
        return id_to;
    }

    public void setId_to(int id_to) {
        this.id_to = id_to;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
