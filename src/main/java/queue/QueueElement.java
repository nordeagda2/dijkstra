package queue;

/**
 * Created by amen on 9/7/17.
 */
public class QueueElement<V> {
    private V data;
    private Double priority;

    public QueueElement(V data, Double priority) {
        this.data = data;
        this.priority = priority;
    }

    public V getData() {
        return data;
    }

    public void setData(V data) {
        this.data = data;
    }

    public Double getPriority() {
        return priority;
    }

    public void setPriority(Double priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "QueueElement{" +
                "data=" + data +
                ", priority=" + priority +
                '}';
    }
}
