package queue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by amen on 9/7/17.
 */
public class MyPriorityQueue<V> {

    private boolean ascending = true;

    private List<QueueElement<V>> elements;

    public MyPriorityQueue() {
        this.elements = new ArrayList<QueueElement<V>>();
    }

    /**
     * Wstawiamy element do kolejki na odpowiednie miejsce (z odpowiednim priorytetem)
     * @param data - element do wstawienia
     * @param priority - jego priorytet
     */
    public void add(V data, Double priority) {
        QueueElement<V> newElement = new QueueElement<V>(data, priority);
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).getPriority() > newElement.getPriority()) {
                elements.add(i, newElement);
                return;
            }
        }

        elements.add(newElement);
    }

    /**
     * Pobiera element ze szczytu kolejki priorytetowej i usuwa go z niej.
     * @return - zwraca szczyt kolejki.
     */
    public V pop() {
        if (elements.size() > 0) {
            return elements.remove(0).getData();
        }

        return null;
    }

    public void print() {
        System.out.println();
        for (int i = 0; i < elements.size(); i++) {
            System.out.print(elements.get(i).getData() + ", ");
        }
        System.out.println();
    }

    public boolean isEmpty() {
        return elements.isEmpty();
    }
}
